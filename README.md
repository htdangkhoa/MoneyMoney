# MoneyMoney App

## Git flow
- First run.  
`git flow init`  
`git checkout develop`  
- When you start code a new feature, you must create new feature branch.  
`git flow feature start <your_name_feature>`  
- Then you finish feature, run this.  
`git add -A`  
`git commit -m <commit_message>`  
`git pull origin feature/<your_name_feature> develop`  
`git push origin feature/<your_name_feature>`  
- Merge to develop branch.  
`git flow feature finish <your_name_feature>`  
`git push origin develop`  
- Merge to master branch.  
`git checkout master`  
`git pull origin master`  
`git merge develop`  
`git push origin master`  