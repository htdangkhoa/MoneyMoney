package com.dangkhoa.moneymoney;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ms_square.etsyblur.BlurSupport;
import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Fragment.AddNewCard;
import Fragment.NewTransferFragment;
import Fragment.TransferFragment;
import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.StaticClass;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import Fragment.CardManageFragment;
import Fragment.ConvertFragment;
import Fragment.ExpenseIncomeFragment;
import Fragment.NoteFragment;
import Fragment.SettingsFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RelativeLayout moneyNav;
    CircleImageView nav_avatar;
    TextView nav_name, nav_email;

    public static DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initialize(navigationView);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        BlurSupport.addTo(drawer);

        String image = StaticClass.getData(getApplicationContext(), "avatar");
        if (!image.isEmpty()) {
            Logger.d("IMAGE: " + image);
            nav_avatar.setBackgroundColor(getResources().getColor(R.color.transparent));
            nav_avatar.setImageDrawable(StaticClass.ConvertBase64ToDrawable(getApplicationContext(), image));
        }
        nav_name.setText(StaticClass.getData(getApplicationContext(), "name"));
        nav_email.setText(StaticClass.getData(getApplicationContext(), "email"));

        String token = StaticClass.getData(getApplicationContext(), "token");
        RequestParams params = new RequestParams();
        params.put("id", StaticClass.getData(getApplicationContext(), "id"));

        if (StaticClass.getData(getApplicationContext(), "STATE") != null && StaticClass.getData(getApplicationContext(), "STATE").equals("SETTINGS_FRAGMENT")) {
            StaticClass.GoToNewFragment(getSupportFragmentManager(), new SettingsFragment(), null);
            StaticClass.removeData(getApplicationContext(), "STATE");
        } else {

            AsyncHttp.GET("/cards", params, token, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        JSONArray arrCard = response.getJSONArray("successData");
                        if (arrCard.length() > 0) {
                            StaticClass.GoToNewFragment(getSupportFragmentManager(), new ExpenseIncomeFragment(), null);
                        } else {
                            StaticClass.GoToNewFragment(getSupportFragmentManager(), new AddNewCard(), null);
                        }
                    } catch (JSONException e) {
                        StaticClass.GoToNewFragment(getSupportFragmentManager(), new AddNewCard(), null);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }
            });
        }
//        StaticClass.GoToNewFragment(getSupportFragmentManager(), new TransferFragment(), null);
    }

    private void initialize(NavigationView navigationView) {
        nav_avatar = (CircleImageView) navigationView.getHeaderView(0).findViewById(R.id.nav_avatar);
        nav_name = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_name);
        nav_email = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_email);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            System.exit(0);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_expense: {
                StaticClass.saveData(getApplicationContext(), "type", "expense");
                StaticClass.GoToNewFragment(getSupportFragmentManager(), new ExpenseIncomeFragment(), null);
                break;
            }
            case R.id.nav_income: {
                StaticClass.saveData(getApplicationContext(), "type", "income");
                StaticClass.GoToNewFragment(getSupportFragmentManager(), new ExpenseIncomeFragment(), null);
                break;
            }
            case R.id.nav_cards: {
                StaticClass.GoToNewFragment(getSupportFragmentManager(), new CardManageFragment(), null);
                break;
            }
            case R.id.nav_transfer: {
                StaticClass.GoToNewFragment(getSupportFragmentManager(), new TransferFragment(), null);
                break;
            }
            case R.id.nav_convert: {
                StaticClass.GoToNewFragment(getSupportFragmentManager(), new ConvertFragment(), null);
                break;
            }
            case R.id.nav_note: {
                StaticClass.GoToNewFragment(getSupportFragmentManager(), new NoteFragment(), null);
                break;
            }
            case R.id.nav_settings: {
                StaticClass.GoToNewFragment(getSupportFragmentManager(), new SettingsFragment(), null);
                break;
            }
            default: break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
