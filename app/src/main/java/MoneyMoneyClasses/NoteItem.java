package MoneyMoneyClasses;

/**
 * Created by DELL on 8/19/2017.
 */

public class NoteItem {
    private String Title,Date,Id;

    public NoteItem(String id,String title,String date){
        Id = id;
        Title=title;
        Date=date;
    }

    public String getTitle(){return Title;}

    public void SetTitle(String title){Title=title;}

    public String getDate(){return Date;}

    public void setDate(String date) {
        Date = date;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }
}
