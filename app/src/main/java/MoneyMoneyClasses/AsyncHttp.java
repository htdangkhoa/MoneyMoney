package MoneyMoneyClasses;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.protocol.RequestUserAgent;

/**
 * Created by dangkhoa on 8/15/17.
 */

public class AsyncHttp {
//    private static String host = "http://10.0.2.2:8888/v1";
private static String host = "https://moneymoneyserver.now.sh/v1";
    static AsyncHttpClient client = new AsyncHttpClient();
    static {
        client.setThreadPool(Executors.newSingleThreadScheduledExecutor());
    }

    private static String getAbsoluteUrl(String route) {
        return host + route;
    }

    public static void GET(String route, RequestParams params, String token, AsyncHttpResponseHandler asyncHttpResponseHandler) {
        if (token != null) {
            client.addHeader("Authorization", "JWT " + token);
        }

        client.get(getAbsoluteUrl(route), params, asyncHttpResponseHandler);
    }

    public static void POST(String route, RequestParams params, String token, AsyncHttpResponseHandler asyncHttpResponseHandler) {
        if (token != null) {
            client.addHeader("Authorization", "JWT " + token);
        }

        client.post(getAbsoluteUrl(route), params, asyncHttpResponseHandler);
    }

    public static void PATCH(String route, RequestParams params, String token, AsyncHttpResponseHandler asyncHttpResponseHandler) {
        if (token != null) {
            client.addHeader("Authorization", "JWT " + token);
        }

        client.patch(getAbsoluteUrl(route), params, asyncHttpResponseHandler);
    }

    public static void DELETE(String route, RequestParams params, String token, AsyncHttpResponseHandler asyncHttpResponseHandler) {
        if (token != null) {
            client.addHeader("Authorization", "JWT " + token);
        }

        client.delete(getAbsoluteUrl(route), params, asyncHttpResponseHandler);
    }

    public static void UPLOAD_IMAGE(RequestParams params, AsyncHttpResponseHandler asyncHttpResponseHandler) {
        client.post("http://uploads.im/api", params, asyncHttpResponseHandler);
    }
}
