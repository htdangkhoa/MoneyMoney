package MoneyMoneyClasses;

import android.graphics.drawable.Drawable;

/**
 * Created by dangkhoa on 9/1/17.
 */

public class RecordItem {
    String Id, Date, Time, Type, Card, Category, Value, Note, Picture;
    Drawable Icon;

    public RecordItem(String id, String date, String time, String type, String card, String category, String value, String note, String picture, Drawable icon) {
        Id = id;
        Date = date;
        Time = time;
        Type = type;
        Card = card;
        Category = category;
        Value = value;
        Note = note;
        Picture = picture;
        Icon = icon;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getCard() {
        return Card;
    }

    public void setCard(String card) {
        Card = card;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        Picture = picture;
    }

    public Drawable getIcon() {
        return Icon;
    }

    public void setIcon(Drawable icon) {
        Icon = icon;
    }
}
