package MoneyMoneyClasses;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dangkhoa.moneymoney.R;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

/**
 * Created by dangkhoa on 8/13/17.
 */

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {
    ArrayList<CardItem> cardItems;
    Context context;

    public CardAdapter(ArrayList<CardItem> cardItems, Context context) {
        this.cardItems = cardItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.image.setImageDrawable(cardItems.get(position).getImage());
        holder.name.setText(cardItems.get(position).getName ());
        holder.balance.setText(cardItems.get(position).getBalance());
    }

    @Override
    public int getItemCount() {
        return cardItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RoundedImageView image;
        TextView  name, balance;

        public ViewHolder(View itemView) {
            super(itemView);
            balance = (TextView) itemView.findViewById(R.id.txt_cm_balance);
            image = (RoundedImageView) itemView.findViewById(R.id.img_cm_card);
            name = (TextView) itemView.findViewById(R.id.txt_cm_name);
        }
    }
}
