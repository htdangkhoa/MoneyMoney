package MoneyMoneyClasses;

import android.graphics.drawable.Drawable;

/**
 * Created by dangkhoa on 8/13/17.
 */

public class CardItem {
    Drawable image;
    String name, balance;

    public CardItem(Drawable image,String name,  String balance) {
        this.image = image;
        this.balance = balance;
        this.name=name;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBalance() {
        return balance;
    }
}
