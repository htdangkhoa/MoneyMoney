package MoneyMoneyClasses;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dangkhoa.moneymoney.R;

import java.util.ArrayList;

import Fragment.RecordFragment;

/**
 * Created by dangkhoa on 8/13/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    ArrayList<CategoryItem> categoryItems;
    Context context;

    public CategoryAdapter(ArrayList<CategoryItem> categoryItems, Context context) {
        this.categoryItems = categoryItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();

        holder.txtCategory.setText(categoryItems.get(position).getCategory());
        holder.txtPrice.setText(categoryItems.get(position).getPrice());
        holder.imgIconCategory.setImageDrawable(categoryItems.get(position).getIcon());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("category", String.valueOf(holder.txtCategory.getText()));
                bundle.putString("price", String.valueOf(holder.txtPrice.getText()));
                StaticClass.GoToNewFragmentWithAnimation(fragmentManager, false, new RecordFragment(), bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtCategory, txtPrice;
        ImageView imgIconCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            txtCategory = (TextView) itemView.findViewById(R.id.item_category);
            txtPrice = (TextView) itemView.findViewById(R.id.item_price);
            imgIconCategory = (ImageView) itemView.findViewById(R.id.item_icon_category);
        }
    }
}
