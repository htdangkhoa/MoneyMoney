package MoneyMoneyClasses;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Toast;

import com.dangkhoa.moneymoney.R;
import com.orhanobut.logger.Logger;

/**
 * Created by dangkhoa on 9/21/17.
 */

public abstract class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback {
    Context context;

    Drawable deleteIcon;
    ColorDrawable background = new ColorDrawable();
    int DELETE = Color.parseColor("#CCF02D3A");
    int EDIT = Color.parseColor("#84CAE7");

    public SwipeToDeleteCallback(int dragDirs, int swipeDirs, Context context) {
        super(dragDirs, swipeDirs);
        this.context = context;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if (viewHolder.getAdapterPosition() == -1) {
            return;
        }

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            View itemView = viewHolder.itemView;
            int height = itemView.getBottom() - itemView.getTop();

            if(dX <= 0) {
                background.setColor(DELETE);
                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                // Draw the delete icon
                deleteIcon = context.getResources().getDrawable(R.drawable.ic_remove);
                int intrinsicWidth = deleteIcon.getIntrinsicWidth();
                int intrinsicHeight = deleteIcon.getIntrinsicHeight();

                int deleteIconTop = itemView.getTop() + (height - intrinsicHeight) / 2;
                int deleteIconMargin = (height - intrinsicHeight) / 2;
                int deleteIconLeft = itemView.getRight() - deleteIconMargin - intrinsicWidth;
                int deleteIconRight = itemView.getRight() - deleteIconMargin;
                int deleteIconBottom = deleteIconTop + intrinsicHeight;
                deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom);
                deleteIcon.draw(c);
            }else {
                background.setColor(EDIT);
                background.setBounds((int) dX - itemView.getLeft(), itemView.getTop(), itemView.getLeft(), itemView.getBottom());
                background.draw(c);

                // Draw the edit icon
                deleteIcon = context.getResources().getDrawable(R.drawable.ic_edit);
                int intrinsicWidth = deleteIcon.getIntrinsicWidth();
                int intrinsicHeight = deleteIcon.getIntrinsicHeight();

                int deleteIconTop = itemView.getTop() + (height - intrinsicHeight) / 2;
                int deleteIconMargin = (height - intrinsicHeight) / 2;
                int deleteIconLeft = itemView.getLeft() + deleteIconMargin;
                int deleteIconRight = itemView.getLeft() + deleteIconMargin + intrinsicWidth;
                int deleteIconBottom = deleteIconTop + intrinsicHeight;
                deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom);
                deleteIcon.draw(c);
            }
        }

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }
}
