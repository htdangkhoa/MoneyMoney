package MoneyMoneyClasses;

/**
 * Created by dangkhoa on 9/16/17.
 */

public class ContextMenu {
    String Name, Key, Value;

    public ContextMenu(String key, String value) {
        Key = key;
        Value = value;
    }

    public ContextMenu(String name, String key, String value) {
        Name = name;
        Key = key;
        Value = value;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
