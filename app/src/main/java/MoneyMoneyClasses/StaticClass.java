package MoneyMoneyClasses;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.dangkhoa.moneymoney.MainActivity;
import com.dangkhoa.moneymoney.R;
import com.makeramen.roundedimageview.RoundedDrawable;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by dangkhoa on 8/9/17.
 */

public class StaticClass {
    public static void RequestPermission(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fragment.requestPermissions(new String[] {
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            }, 9999);
        }
    }

    public static ImageButton initNavButton(final Context context, View view) {
        ImageButton button = new ImageButton(context);
        button.setMinimumWidth(30);
        button.setMinimumHeight(30);
        button.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_menu));
        button.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(40, 22, 40, 22);

        button.setLayoutParams(params);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.drawer.openDrawer(Gravity.LEFT);
            }
        });

        return  button;
    }

    public static void GoToNewFragmentWithAnimation(FragmentManager fragmentManager, boolean isBacking, Fragment fragment, Bundle bundle) {
        Fragment oldFragment = fragmentManager.findFragmentByTag(fragmentManager.getClass().getName());
        if (oldFragment != null) {
            fragmentManager
                    .beginTransaction()
                    .remove(oldFragment)
                    .commit();
        }

        if (bundle != null) {
            fragment.setArguments(bundle);
        }

        if (isBacking) {
            fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right)
                    .replace(R.id.content_frame, fragment)
                    .commit();
        }else {
            fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.content_frame, fragment)
                    .commit();
        }
    }

    public static void GoToNewFragment(FragmentManager fragmentManager, Fragment fragment, Bundle bundle) {
        Fragment oldFragment = fragmentManager.findFragmentByTag(fragmentManager.getClass().getName());
        if (oldFragment != null) {
            fragmentManager
                    .beginTransaction()
                    .remove(oldFragment)
                    .commit();
        }

        if (bundle != null) {
            fragment.setArguments(bundle);
        }

        fragmentManager
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }

    public static String ConvertDrawableToBase64(Drawable drawable) {
        if (drawable == null) {
            return "";
        }

        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }

    public static String ConvertDrawableToBase64(RoundedDrawable drawable) {
        if (drawable == null) {
            return "";
        }

        Bitmap bitmap = drawable.toBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }

    public static Drawable ConvertBase64ToDrawable(Context context, String base64) {
        if (base64.isEmpty() || base64 == null) {
            return null;
        }

        byte[] decodedString = Base64.decode(base64.replace("data:image/png;base64,", ""), Base64.NO_WRAP);
        Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        Drawable drawable = new BitmapDrawable(context.getResources(), bitmap);
        return drawable;
    }

    public static void ShowAlertDialog(Context context, String title, String message, String negativeButton, DialogInterface.OnClickListener negativeButtonOnClickListener, String positiveButton, DialogInterface.OnClickListener positiveButtonOnClickListener ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);

        if (negativeButton != null) {
            builder.setNegativeButton(negativeButton, negativeButtonOnClickListener);
        }

        if (positiveButton != null) {
            builder.setPositiveButton(positiveButton, positiveButtonOnClickListener);
        }

        builder.show();
    }

    public static void saveData(Context context, String key, String value){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value).commit();
    }

    public static String getData(Context context, String key){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (sharedPreferences == null) {
            return null;
        }

        return sharedPreferences.getString(key, null);
    }

    public static void removeData(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key).commit();
    }

    public static void removeAllData(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().commit();
    }

    public static String getToken(Context context) {
        String token = getData(context, "token");
        if (token != null) {
            return token;
        }

        return null;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static Drawable getIconDetail(Context context, String name) {
        switch (name.toLowerCase()) {
            case "food": {
                return context.getResources().getDrawable(R.drawable.ic_expense_food);
            }
            case "education": {
                return context.getResources().getDrawable(R.drawable.ic_expense_education);
            }
            case "sport": {
                return context.getResources().getDrawable(R.drawable.ic_expense_sport);
            }
            case "household": {
                return context.getResources().getDrawable(R.drawable.ic_expense_household);
            }
            case "health": {
                return context.getResources().getDrawable(R.drawable.ic_expense_health);
            }
            case "travel": {
                return context.getResources().getDrawable(R.drawable.ic_expense_travel);
            }
            case "fashion": {
                return context.getResources().getDrawable(R.drawable.ic_expense_fashion);
            }
            case "transport": {
                return context.getResources().getDrawable(R.drawable.ic_expense_transport);
            }
            case "etc": {
                return context.getResources().getDrawable(R.drawable.ic_expense_etc);
            }
            default: {
                return context.getResources().getDrawable(R.drawable.ic_expense_etc);
            }
        }
    }

    public static Drawable matchFlag(Context context, String country) {
        switch (country) {
            default: return null;
        }
    }

    public static String ConvertStringToDecimal(Object num) {
        DecimalFormatSymbols symbol = new DecimalFormatSymbols();
        symbol.setDecimalSeparator(',');
        symbol.setGroupingSeparator('.');
        DecimalFormat format = (DecimalFormat) NumberFormat.getInstance();
        format.setDecimalFormatSymbols(symbol);

        if (num instanceof String) {
            if ((String) num != null ) {
                return format.format(Long.parseLong((String) num)).replaceAll("[$,.]", ",");
            }
        }

        return format.format(num).replaceAll("[$,.]", ",");
    }

    public static String ConvertDecimalToString(Object s) {
        if (s instanceof String) {
            return ((String) s).replaceAll("[$,.]", "");
        }

        return String.valueOf(s).replaceAll("[$,.]", "");
    }
}