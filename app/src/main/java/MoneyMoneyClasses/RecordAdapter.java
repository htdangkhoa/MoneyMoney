package MoneyMoneyClasses;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dangkhoa.moneymoney.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by dangkhoa on 8/13/17.
 */

public class RecordAdapter extends RecyclerView.Adapter<RecordAdapter.ViewHolder> {
    ArrayList<RecordItem> recordItems;
    Context context;

    public RecordAdapter(ArrayList<RecordItem> recordItems, Context context) {
        this.recordItems = recordItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.record, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.imgIconRecord.setImageDrawable(recordItems.get(position).getIcon());
        holder.txtRecordCategory.setText(recordItems.get(position).getCategory());
        holder.txtRecordPrice.setText(recordItems.get(position).getValue());
        holder.txtRecordDate.setText(recordItems.get(position).getDate());
        holder.txtRecordNote.setText(recordItems.get(position).getNote());

        if (recordItems.get(position).getNote().isEmpty()) {
            holder.txtRecordNote.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return recordItems.size();
    }

    public void removeItem(int position) {
        RequestParams params = new RequestParams();
        params.put("id", recordItems.get(position).getId ());
        AsyncHttp.DELETE("/record/delete", params, StaticClass.getToken(context), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.d("DELETE: " + response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
        recordItems.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, recordItems.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtRecordCategory, txtRecordPrice, txtRecordDate, txtRecordNote;
        ImageView imgIconRecord;

        public ViewHolder(View itemView) {
            super(itemView);
            imgIconRecord = (ImageView) itemView.findViewById(R.id.item_icon_record);
            txtRecordCategory = (TextView) itemView.findViewById(R.id.item_record_category);
            txtRecordDate = (TextView) itemView.findViewById(R.id.item_record_date);
            txtRecordPrice = (TextView) itemView.findViewById(R.id.item_record_price);
            txtRecordNote = (TextView) itemView.findViewById(R.id.item_record_note);
        }
    }
}