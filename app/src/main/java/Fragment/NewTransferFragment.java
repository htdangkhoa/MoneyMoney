package Fragment;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.dangkhoa.moneymoney.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.ContextMenuAdapter;
import MoneyMoneyClasses.StaticClass;
import cz.msebera.android.httpclient.Header;

/**
 * Created by dangkhoa on 9/29/17.
 */

public class NewTransferFragment extends android.support.v4.app.Fragment implements View.OnClickListener {
    EditText edtTransferFrom, edtTransferTo, edtTransferValue;
    ImageButton btnTransferDone, btnBackToTransform;

    ListView listViewFrom, listViewTo;
    AlertDialog dialog;
    RequestParams params = new RequestParams();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_transfer, container, false);

        initialize(view);

        return view;
    }

    private void initialize(View view) {
        edtTransferFrom = (EditText) view.findViewById(R.id.edtTransferFrom);
        edtTransferTo = (EditText)  view.findViewById(R.id.edtTransferTo);
        edtTransferValue = (EditText) view.findViewById(R.id.edtTransferValue);
        btnTransferDone = (ImageButton) view.findViewById(R.id.btnTransferDone);
        btnBackToTransform = (ImageButton) view.findViewById(R.id.btnBackToTransform);

        edtTransferFrom.setOnClickListener(this);
        edtTransferTo.setOnClickListener(this);
        btnTransferDone.setOnClickListener(this);
        btnBackToTransform.setOnClickListener(this);

        edtTransferValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = StaticClass.ConvertDecimalToString(editable.toString());
                edtTransferValue.removeTextChangedListener(this);

                if (!s.isEmpty()) {
                    String formatted = StaticClass.ConvertStringToDecimal(s);
                    edtTransferValue.setText(formatted);
                    edtTransferValue.setSelection(formatted.length());
                }

                edtTransferValue.addTextChangedListener(this);
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View popup = getActivity().getLayoutInflater().inflate(R.layout.custom_context_menu, null);
        builder.setView(popup);
        dialog = builder.create();
        listViewFrom = (ListView) popup.findViewById(R.id.listView);
        listViewTo = (ListView) popup.findViewById(R.id.listView);
        final ContextMenuAdapter adapter = new ContextMenuAdapter(getContext(), R.layout.context_menu, CardManageFragment.cards);
        listViewFrom.setAdapter(adapter);
        listViewTo.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.edtTransferFrom: {
                listViewFrom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        edtTransferFrom.setText(CardManageFragment.cards.get(i).getKey());
                        params.put("from", CardManageFragment.cards.get(i).getValue());
                        params.put("namef", CardManageFragment.cards.get(i).getName());
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            }
            case R.id.edtTransferTo: {
                listViewTo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        edtTransferTo.setText(CardManageFragment.cards.get(i).getKey());
                        params.put("to", CardManageFragment.cards.get(i).getValue());
                        params.put("namet", CardManageFragment.cards.get(i).getName());
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            }
            case R.id.btnBackToTransform: {
                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new TransferFragment(), null);
                break;
            }
            case R.id.btnTransferDone: {
                params.put("value", StaticClass.ConvertDecimalToString(edtTransferValue.getText()));
                AsyncHttp.POST("/transfer", params, StaticClass.getToken(getContext()), new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new TransferFragment(), null);
                        StaticClass.hideSoftKeyboard(getActivity());
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                    }
                });
                break;
            }
        }
    }
}
