package Fragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.dangkhoa.moneymoney.BuildConfig;
import com.dangkhoa.moneymoney.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.makeramen.roundedimageview.RoundedDrawable;
import com.makeramen.roundedimageview.RoundedImageView;
import com.orhanobut.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.StaticClass;
import cz.msebera.android.httpclient.Header;
import io.card.payment.CardIOActivity;

import static android.app.Activity.RESULT_OK;

/**
 * Created by DELL on 8/31/2017.
 */

public class AddNewCard extends android.support.v4.app.Fragment implements View.OnClickListener {
    ImageView ic_camera;
    RoundedImageView picture;
    ImageButton btnBackToCard, btnAddCard;
    EditText newCardType, newCardHolder, newCardExp, newCardNumber, newCardCvv, newCardBalance;
    int IMAGE_GALLERY_REQUEST = 20;
    int SCAN_REQUEST_CODE = 99;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.fragment_add_new_card,container,false);
        initialize ( view );

        registerForContextMenu(picture);
        picture.setOnClickListener(this);

        registerForContextMenu(newCardType);
        newCardType.setOnClickListener(this);

        btnBackToCard.setOnClickListener(this);
        btnAddCard.setOnClickListener(this);

        newCardExp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int index, int deleted, int i2) {
                Logger.d("EXP: " + String.valueOf(charSequence) + "|" + String.valueOf(index) + "|" + String.valueOf(deleted) + "|" + String.valueOf(i2));

                int length = String.valueOf(charSequence).length();
                if (length == 2) {
                    if (deleted == 1) {
                        newCardExp.setText(String.valueOf(charSequence).substring(0, String.valueOf(charSequence).length() - 1));
                    }else {
                        newCardExp.setText(String.valueOf(charSequence) + "/");
                    }
                }

                if (length > 0 && length != 3 && String.valueOf(charSequence).charAt(length-1) == '/') {
                    newCardExp.setText(String.valueOf(charSequence).substring(0, String.valueOf(charSequence).length() - 1));
                }

                newCardExp.setSelection(newCardExp.getText().length());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    private void initialize(View view){
        StaticClass.RequestPermission(this);

        picture = (RoundedImageView) view.findViewById(R.id.take_picture);
        ic_camera = (ImageView) view.findViewById(R.id.ic_camera);
        btnBackToCard = (ImageButton) view.findViewById (R.id.btnBackCard);
        btnAddCard = (ImageButton) view.findViewById(R.id.btnAddCard);
        newCardType = (EditText) view.findViewById(R.id.newCardType);
        newCardHolder = (EditText) view.findViewById(R.id.newCardHolder);
        newCardBalance = (EditText) view.findViewById(R.id.newCardBalance);
        newCardNumber = (EditText) view.findViewById(R.id.newCardNumber);
        newCardExp = (EditText) view.findViewById(R.id.newCardExp);
        newCardCvv = (EditText) view.findViewById(R.id.newCardCvv);

        newCardBalance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = StaticClass.ConvertDecimalToString(editable.toString());
                newCardBalance.removeTextChangedListener(this);

                if (!s.isEmpty()) {
                    String formatted = StaticClass.ConvertStringToDecimal(s);
                    newCardBalance.setText(formatted);
                    newCardBalance.setSelection(formatted.length());
                }

                newCardBalance.addTextChangedListener(this);
            }
        });
    }

    private void openCamera(Fragment fragment) {
        Intent scanIntent = new Intent(getContext(), CardIOActivity.class);
        scanIntent.putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, false);
        scanIntent.putExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_CAPTURED_CARD_IMAGE, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_SCAN_RESULT, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_SCAN, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, getResources().getColor(R.color.colorPrimary));

        fragment.startActivityForResult(scanIntent, SCAN_REQUEST_CODE);
    }

    private void openGallery() {
        Intent intent = new Intent ( Intent.ACTION_PICK );

        File pictureDirectory = Environment.getExternalStoragePublicDirectory (Environment.DIRECTORY_PICTURES  );
        String pictureDirectoryPath = pictureDirectory.getPath();
        // finally, get a URI representation
        Uri data = Uri.parse(pictureDirectoryPath);

        // set the data and type.  Get all image types.
        intent.setDataAndType(data, "image/*");

        // we will invoke this activity, and get something back from it.
        startActivityForResult(intent, IMAGE_GALLERY_REQUEST);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getActivity().getMenuInflater();

        switch (v.getId()) {
            case R.id.take_picture:
                inflater.inflate(R.menu.menu_context_image, menu);
                break;
            case R.id.newCardType:
                inflater.inflate(R.menu.menu_add_new_card, menu);
                break;
            default: break;
        }

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menuCreditCard:
                newCardType.setText("Credit card");
                break;
            case R.id.menuNormalCard:
                newCardType.setText("Normal card");
                break;
            case R.id.menuOtherCard:
                newCardType.setText("Other card");
                break;
            case R.id.camera: {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getContext().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        openCamera(this);
                    }else {
                        StaticClass.ShowAlertDialog(getContext(), "Warning", "You should allow app use the camera permission.", null, null, "Open Settings", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID)));
                            }
                        });
                    }
                }else {
                    openCamera(this);
                }
                break;
            }
            default: {
                openGallery();
                break;
            }
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if we are here, everything processed successfully.
        if (requestCode == SCAN_REQUEST_CODE && data != null) {
            picture.setImageBitmap(CardIOActivity.getCapturedCardImage(data));
            ic_camera.setVisibility(View.INVISIBLE);
        }

        if (resultCode == RESULT_OK && requestCode == IMAGE_GALLERY_REQUEST) {
            // if we are here, we are hearing back from the image gallery.

            // the address of the image on the SD Card.
            Uri imageUri = data.getData();
            // declare a stream to read the image data from the SD Card.
            InputStream inputStream;

            // we are getting an input stream, based on the URI of the image.
            try {
                inputStream = getContext().getContentResolver().openInputStream(imageUri);

                // get a bitmap from the stream.
                Bitmap image = BitmapFactory.decodeStream(inputStream);

                // show the image to the user
                Drawable dr = new BitmapDrawable(getContext().getResources(), image);
                picture.setBackground(dr);
                ic_camera.setVisibility(View.INVISIBLE);
            } catch (FileNotFoundException e) {
                e.printStackTrace ( );
            }
        }
    }

    @Override
    public void onClick(View view) {
        StaticClass.hideSoftKeyboard(getActivity());

        int id = view.getId();

        switch (id) {
            case R.id.btnBackCard: {
                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new CardManageFragment(), null);
                break;
            }
            case R.id.btnAddCard: {
                RequestParams params = new RequestParams();
                params.put("id", StaticClass.getData(getContext(), "id"));
                if (picture.getDrawable() != null) {
                    Logger.d("IMAGE: " + StaticClass.ConvertDrawableToBase64((RoundedDrawable) picture.getDrawable()));
                    params.put("image", StaticClass.ConvertDrawableToBase64((RoundedDrawable) picture.getDrawable()));
                }
                params.put("type", newCardType.getText());
                params.put("start", StaticClass.ConvertDecimalToString(newCardBalance.getText()));
                params.put("name", newCardHolder.getText());
                params.put("number", newCardNumber.getText());
                params.put("exp", newCardExp.getText());
                params.put("cvv", newCardCvv.getText());

                AsyncHttp.POST("/card/create", params, StaticClass.getToken(getContext()), new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Logger.d("RESULT: " + response);
                        try {
                            StaticClass.ShowAlertDialog(getContext(), "Success", response.getString("successData"), null, null, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new CardManageFragment(), null);
                                }
                            });
                        } catch (JSONException e) {
                            try {
                                StaticClass.ShowAlertDialog(getContext(), "Error", response.getString("errorMessage"), null, null, "OK", null);
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        Logger.e("ERROR: " + errorResponse);
                    }
                });

                break;
            }
            case R.id.newCardType: case R.id.take_picture:
                getActivity().openContextMenu(view);
                break;
        }
    }
}
