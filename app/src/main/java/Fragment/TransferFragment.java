package Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.dangkhoa.moneymoney.R;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.StaticClass;
import MoneyMoneyClasses.TransferAdapter;
import MoneyMoneyClasses.TransferItem;
import cz.msebera.android.httpclient.Header;

/**
 * Created by dangkhoa on 10/20/17.
 */

public class TransferFragment extends Fragment implements View.OnClickListener {
    RelativeLayout moneyNav;
    RecyclerView recycler_view_transfer;
    ImageButton btnAddTransfer;

    ArrayList<TransferItem> transferItems = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer, container, false);

        initialize(view);

        return view;
    }

    private void initialize(View view) {
        moneyNav = (RelativeLayout) view.findViewById(R.id.moneyNav);
        moneyNav.addView(StaticClass.initNavButton(view.getContext(), view));
        recycler_view_transfer = (RecyclerView) view.findViewById(R.id.recycler_view_transfer);
        btnAddTransfer = (ImageButton) view.findViewById(R.id.btnAddTransfer);

        btnAddTransfer.setOnClickListener(this);

        AsyncHttp.GET("/transfer", null, StaticClass.getToken(getContext()), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray array = response.getJSONArray("successData");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        transferItems.add(new TransferItem(object.getString("from"), object.getString("to"), object.getString("datetime"), String.valueOf(object.getInt("value"))));
                    }
                    setupRecyclerView(recycler_view_transfer);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.custom_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);

        TransferAdapter adapter = new TransferAdapter(transferItems, getContext());
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btnAddTransfer: {
                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), false, new NewTransferFragment(), null);
                break;
            }
        }
    }
}
