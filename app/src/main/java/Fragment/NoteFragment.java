package Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.baoyz.widget.PullRefreshLayout;
import com.dangkhoa.moneymoney.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.CategoryAdapter;
import MoneyMoneyClasses.CategoryItem;
import MoneyMoneyClasses.NoteAdapter;
import MoneyMoneyClasses.NoteItem;
import MoneyMoneyClasses.RecordAdapter;
import MoneyMoneyClasses.StaticClass;
import MoneyMoneyClasses.SwipeToDeleteCallback;
import cz.msebera.android.httpclient.Header;

import static com.dangkhoa.moneymoney.R.id.pullRefreshLayout;
import static com.dangkhoa.moneymoney.R.id.title;

/**
 * Created by DELL on 8/19/2017.
 */

public class NoteFragment extends android.support.v4.app.Fragment{
    RelativeLayout moneyNav;
    RecyclerView recyclerView;
    ImageButton imageButton;
    PullRefreshLayout pullRefreshLayout;
    RelativeLayout dialogLoading;
    String route;
    RequestParams params = new RequestParams();
    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_note, container, false);

        initialize(view);
        imageButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), false, new AddNoteFragment(), null);
            }
        });
        return  view;
    }
    private void initialize(View view) {
        moneyNav = (RelativeLayout) view.findViewById(R.id.moneyNav);
        moneyNav.addView(StaticClass.initNavButton(view.getContext(), view));
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_note);
        imageButton = (ImageButton)view.findViewById(R.id.btnAddNote);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefreshLayout);
        dialogLoading = (RelativeLayout) view.findViewById(R.id.dialogLoading);
        route = "/notes/" ;
        requestToServer(route);
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullRefreshLayout.setRefreshing(true);
                requestToServer(route);
            }
        });
    }
    private void setUpRecyclerViewList(@NonNull RecyclerView recyclerView, @NonNull JSONArray array) {
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.custom_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);

        ArrayList<NoteItem> noteItems = new ArrayList<>();
//        noteItems.add(new NoteItem("Urban Station Coffee", "jhvjhvcgvgj"));
//        noteItems.add(new NoteItem("Urban Station Coffee", "jhvjhvcgvgj"));
//        noteItems.add(new NoteItem("Urban Station Coffee", "jhvjhvcgvgj"));
//        noteItems.add(new NoteItem("Urban Station Coffee", "jhvjhvcgvgj"));
//        noteItems.add(new NoteItem("Urban Station Coffee", "jhvjhvcgvgj"));
//        noteItems.add(new NoteItem("Urban Station Coffee", "jhvjhvcgvgj"));
//        noteItems.add(new NoteItem("Urban Station Coffee", "jhvjhvcgvgj"));
//        noteItems.add(new NoteItem("Urban Station Coffee", "jhvjhvcgvgj"));
//        noteItems.add(new NoteItem("Urban Station Coffee", "jhvjhvcgvgj"));
//        noteItems.add(new NoteItem("Urban Station Coffee", "jhvjhvcgvgj"));
//        noteItems.add(new NoteItem("Urban Station Coffee", "jhvjhvcgvgj"));
        for (int i = 0; i < array.length(); i++){
            try{
                JSONObject object = array.getJSONObject(i);

                noteItems.add ( new NoteItem ( object.getString("_id"), object.getString ( "title"),object.getString ( "content" ) ) );
            }catch (JSONException e) {
                e.printStackTrace();
            }
        }



        final NoteAdapter noteAdapter = new NoteAdapter(noteItems, getContext());
        recyclerView.setAdapter(noteAdapter);

        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(0, ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT, getContext()) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.LEFT) {
                    noteAdapter.removeItem(viewHolder.getAdapterPosition());
                }else {
                    StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), false, new AddNoteFragment (), null);
                }
            }
        };
        ItemTouchHelper helper = new ItemTouchHelper(swipeToDeleteCallback);
        helper.attachToRecyclerView(recyclerView);
    }

    private void requestToServer(@NonNull String route) {
        params.put("id", StaticClass.getData(getContext(), "id"));

        AsyncHttp.GET(route, params, StaticClass.getToken(getContext()), new JsonHttpResponseHandler () {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                dialogLoading.setVisibility(View.INVISIBLE);
                pullRefreshLayout.setRefreshing(false);
                Logger.d("NOTE: " + response);
                try {
                    JSONArray arrRecords = response.getJSONArray("successData");
                    setUpRecyclerViewList(recyclerView, arrRecords);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                pullRefreshLayout.setRefreshing(false);
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }
}
