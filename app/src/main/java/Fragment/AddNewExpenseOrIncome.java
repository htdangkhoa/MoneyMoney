package Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TimePicker;

import com.dangkhoa.moneymoney.BuildConfig;
import com.dangkhoa.moneymoney.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.makeramen.roundedimageview.RoundedImageView;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.sql.Date;

import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.ContextMenuAdapter;
import MoneyMoneyClasses.StaticClass;
import cz.msebera.android.httpclient.Header;

import static android.app.Activity.RESULT_OK;

/**
 * Created by DELL on 8/25/2017.
 */

public class AddNewExpenseOrIncome extends android.support.v4.app.Fragment implements View.OnClickListener {
    ImageView btnTakePic;
    RoundedImageView picture;
    ImageButton btnBackToHome, btnAddNew;
    EditText new_calendar, new_clock, new_type, new_card, new_category, new_value, new_description;

    String type;
    int REQUEST_CODE_CAMERA = 123;
    int IMAGE_GALLERY_REQUEST = 20;
    String img;
    String state, category;
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat sdfForDate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdfForTime = new SimpleDateFormat("HH:mm");
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    RequestParams params = new RequestParams();

    /* EDITING */
    boolean isEditing = false;
    String editingId, editingCard;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_add_new_ex_or_in, container, false);

        final String typeBundle = StaticClass.getData(getContext(), "type");
        Logger.d("BUNDLE: " + StaticClass.getData(getContext(), "type"));
        if (typeBundle != null) {
            type = StaticClass.getData(getContext(), "type");
        }

        initialize(view);

        Bundle bundle = getArguments();
        Logger.d(bundle);
        if (bundle != null) {
            state = bundle.getString("state");
            category = bundle.getString("category");

            if (state.equals("RECORD_EDIT")) {
                new_calendar.setText(bundle.getString("date"));
                new_clock.setText(bundle.getString("time").substring(0, bundle.getString("time").length()-3));
                new_type.setText(StringUtils.capitalize(bundle.getString("type")));
                //card
                for (int i = 0; i < CardManageFragment.cards.size(); i++) {
                    Logger.d(CardManageFragment.cards.get(i));
                    if (bundle.getString("card").equals(CardManageFragment.cards.get(i).getValue())) {
                        new_card.setText(CardManageFragment.cards.get(i).getKey());
                        editingCard = CardManageFragment.cards.get(i).getValue();
                        break;
                    }
                }
                new_category.setText(bundle.getString("category"));
                new_value.setText(bundle.getString("value"));
                new_description.setText(bundle.getString("note"));
                if (URLUtil.isValidUrl(bundle.getString("picture"))) {
                    Picasso
                            .with(getContext())
                            .load(bundle.getString("picture"))
                            .into(picture);
                    btnTakePic.setVisibility(View.GONE);
                }else {
                    picture.setImageDrawable(StaticClass.ConvertBase64ToDrawable(getContext(), bundle.getString("picture")));
                    if (picture.getDrawable() != null) {
                        btnTakePic.setVisibility(View.GONE);
                    }
                }

                isEditing = bundle.getBoolean("isEditing");
                editingId = bundle.getString("id");
            }
        }

        btnBackToHome.setOnClickListener(this);
        btnAddNew.setOnClickListener(this);
        picture.setOnClickListener(this);
        new_type.setOnClickListener(this);
        new_calendar.setOnClickListener(this);
        new_clock.setOnClickListener(this);

        return view;
    }

    private void initialize(View view){
        StaticClass.RequestPermission(this);

        btnTakePic = (ImageView) view.findViewById(R.id.ic_camera);
        picture = (RoundedImageView) view.findViewById(R.id.take_picture);
        btnBackToHome = (ImageButton) view.findViewById(R.id.btnBackToHome);
        btnAddNew = (ImageButton) view.findViewById(R.id.btnAddNew);

        new_calendar = (EditText) view.findViewById(R.id.new_calendar);
        new_clock = (EditText) view.findViewById(R.id.new_clock);
        new_type = (EditText) view.findViewById(R.id.new_type);
        new_card = (EditText) view.findViewById(R.id.new_card);
        new_category = (EditText) view.findViewById(R.id.new_category);
        new_value = (EditText) view.findViewById(R.id.new_value);
        new_description = (EditText) view.findViewById(R.id.new_description);

        picture.setLongClickable(false);
        new_type.setLongClickable(false);
        new_card.setOnClickListener(this);

        registerForContextMenu(picture);
        registerForContextMenu(new_type);
        registerForContextMenu(new_card);

        picture.setLongClickable(false);
        new_type.setLongClickable(false);
        new_card.setLongClickable(false);

        new_value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = StaticClass.ConvertDecimalToString(editable.toString());
                new_value.removeTextChangedListener(this);

                if (!s.isEmpty()) {
                    String formatted = StaticClass.ConvertStringToDecimal(s);
                    new_value.setText(formatted);
                    new_value.setSelection(formatted.length());
                }

                new_value.addTextChangedListener(this);
            }
        });


        new_calendar.setText(sdfForDate.format(new Date(new java.util.Date().getTime())));
        new_clock.setText(sdfForTime.format(new Date(new java.util.Date().getTime())));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View popup = getActivity().getLayoutInflater().inflate(R.layout.custom_context_menu, null);
        builder.setView(popup);
        final AlertDialog dialog = builder.create();

        final ListView listView = (ListView) popup.findViewById(R.id.listView);

        final ContextMenuAdapter adapter = new ContextMenuAdapter(getContext(), R.layout.context_menu, CardManageFragment.cards);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                new_card.setText(CardManageFragment.cards.get(i).getKey());
                params.put("card", CardManageFragment.cards.get(i).getValue());
                dialog.dismiss();
            }
        });

        MenuInflater inflater = getActivity().getMenuInflater();

        switch (v.getId()) {
            case R.id.take_picture:
                inflater.inflate(R.menu.menu_context_image, menu);
                break;
            case R.id.new_type:
                inflater.inflate(R.menu.menu_add_new, menu);
                break;
            case R.id.new_card:
                dialog.show();
                break;
        }

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        File pictureDirectory = Environment.getExternalStoragePublicDirectory (Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();
        Uri data = Uri.parse(pictureDirectoryPath);

        switch (id) {
            case R.id.menuExpense:
                type = "expense";
                StaticClass.saveData(getContext(), "type", type);
                new_type.setText(StringUtils.capitalize(String.valueOf(item.getTitle())));
                break;
            case R.id.menuIncome:
                type = "income";
                StaticClass.saveData(getContext(), "type", type);
                new_type.setText(StringUtils.capitalize(String.valueOf(item.getTitle())));
                break;
            case R.id.camera:
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getContext().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        startActivityForResult(intent, REQUEST_CODE_CAMERA);
                    }else {
                        StaticClass.ShowAlertDialog(getContext(), "Warning", "You should allow app use the camera permission.", null, null, "Open Settings", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID)));
                            }
                        });
                    }
                }else {
                    startActivityForResult(intent, REQUEST_CODE_CAMERA);
                }
                break;
            case R.id.gallery:
                intent = new Intent (Intent.ACTION_PICK);
                intent.setDataAndType(data, "image/*");

                // we will invoke this activity, and get something back from it.
                startActivityForResult(intent, IMAGE_GALLERY_REQUEST);
                break;
        }
        return super.onContextItemSelected(item);
    }

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if we are here, everything processed successfully.
        if (resultCode == RESULT_OK) {
            Uri tempUri = null;
            if (requestCode == IMAGE_GALLERY_REQUEST) {
                // the address of the image on the SD Card.
                tempUri = data.getData();

                // declare a stream to read the image data from the SD Card.
                InputStream inputStream;

                // we are getting an input stream, based on the URI of the image.
                try {
                    inputStream = getContext().getContentResolver().openInputStream(tempUri);
                    // get a bitmap from the stream.
                    Bitmap image = BitmapFactory.decodeStream(inputStream);
                    // show the image to the user
                    Drawable dr = new BitmapDrawable (getContext().getResources(), image);
                    picture.setImageDrawable(dr);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

            if(requestCode == REQUEST_CODE_CAMERA){
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                tempUri = getImageUri(photo);
                Drawable dr = new BitmapDrawable(getContext().getResources(), photo);
                picture.setImageDrawable(dr);
            }
            
            RequestParams upload = new RequestParams();
            try {
                upload.put("upload", new File(getRealPathFromURI(tempUri)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            AsyncHttp.UPLOAD_IMAGE(upload, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        JSONObject object = response.getJSONObject("data");
                        img = object.getString("img_url");
                        params.put("picture", img);
                    } catch (JSONException e) {
                        params.put("picture", StaticClass.ConvertDrawableToBase64(picture.getDrawable()));
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Logger.d(errorResponse);
                }
            });

            btnTakePic.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        StaticClass.hideSoftKeyboard(getActivity());

        int viewId = view.getId();

        switch (viewId) {
            case R.id.btnBackToHome: {
                Bundle bundle = new Bundle();
                bundle.putString("category", category);
                if (state.equals("RECORD_EDIT") || state.equals("RECORD_NEW")) {
                    StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new RecordFragment(), bundle);
                } else {
                    StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new ExpenseIncomeFragment(), null);
                }
                break;
            }
            case R.id.btnAddNew: {
                String token = StaticClass.getData(getContext(), "token");
                String id = StaticClass.getData(getContext(), "id");
                String datetime;

                try {
                    calendar.setTime(sdf.parse(String.valueOf(new_calendar.getText()) + " " + String.valueOf(new_clock.getText())));
                    datetime = String.valueOf(calendar.getTimeInMillis() / 1000);
                } catch (ParseException e) {
                    e.printStackTrace();
                    datetime = String.valueOf(System.currentTimeMillis() / 1000);
                }

                params.put("id", id);
                params.put("datetime", datetime);
                params.put("mode", new_type.getText());
                params.put("category", new_category.getText());
                params.put("value", StaticClass.ConvertDecimalToString(new_value.getText()));
                params.put("note", new_description.getText());

                final Bundle bundle = new Bundle();
                bundle.putString("category", String.valueOf(new_category.getText()));

                if (!isEditing) {
                    AsyncHttp.POST("/record/create", params, token, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Logger.d("RESULT: " + response);
                            if (state.equals("RECORD_EDIT") || state.equals("RECORD_NEW")) {
                                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new RecordFragment(), bundle);
                            } else {
                                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new ExpenseIncomeFragment(), null);
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            Logger.e("ERROR: " + errorResponse);
                            try {
                                StaticClass.ShowAlertDialog(getContext(), "Error", errorResponse.getString("errorMessage"), null, null, "OK", null);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }else {
                    params.put("id", editingId);
                    params.put("card", editingCard);
                    AsyncHttp.PATCH("/record/edit", params, token, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Logger.d("RESULT: " + response);
                            if (state.equals("RECORD_EDIT") || state.equals("RECORD_NEW")) {
                                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new RecordFragment(), bundle);
                            } else {
                                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new ExpenseIncomeFragment(), null);
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            Logger.e("ERROR: " + errorResponse);
                            try {
                                StaticClass.ShowAlertDialog(getContext(), "Error", errorResponse.getString("errorMessage"), null, null, "OK", null);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                break;
            }
            case R.id.new_clock: {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hour);
                        calendar.set(Calendar.MINUTE, minute);
                        calendar.clear(Calendar.SECOND);
                        new_clock.setText(sdfForTime.format(calendar.getTime()));
                    }
                }, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), true);
                timePickerDialog.show();
                break;
            }
            case R.id.new_calendar: {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int date) {
                        calendar.set(year, month, date);
                        new_calendar.setText(sdfForDate.format(calendar.getTime()));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                break;
            }
            default:
                getActivity().openContextMenu(view);
                break;
        }
    }
}
