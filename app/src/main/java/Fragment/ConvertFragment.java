package Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dangkhoa.moneymoney.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import MoneyMoneyClasses.ContextMenuAdapter;
import MoneyMoneyClasses.StaticClass;
import cz.msebera.android.httpclient.Header;

/**
 * Created by dangkhoa on 8/24/17.
 */

public class ConvertFragment extends android.support.v4.app.Fragment implements View.OnClickListener {
    ImageButton btnBackToHome;
    ImageView btnDeleteNumber, btnFlagFrom, btnFlagTo;
    EditText edtFrom, edtTo;
    RelativeLayout btnNum0, btnNum1, btnNum2, btnNum3, btnNum4, btnNum5, btnNum6, btnNum7, btnNum8, btnNum9, btnDot, btnEqual, btnPlus, btnMinus, btnMultiply, btnDivide;
    String btnCalculate = "", valueA = "", valueB = "", result = "";
    boolean first = true;

    ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
    ScriptEngine engine = scriptEngineManager.getEngineByName("rhino");
    ArrayList<MoneyMoneyClasses.ContextMenu> contextMenus = new ArrayList<>();
    AlertDialog dialogFrom, dialogTo;
    String from = "USD", to = "VND";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_convert, container, false);

        initialize(view);

        btnBackToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new ExpenseIncomeFragment(), null);
            }
        });

        btnNum0.setOnClickListener(this);
        btnNum1.setOnClickListener(this);
        btnNum2.setOnClickListener(this);
        btnNum3.setOnClickListener(this);
        btnNum4.setOnClickListener(this);
        btnNum5.setOnClickListener(this);
        btnNum6.setOnClickListener(this);
        btnNum7.setOnClickListener(this);
        btnNum8.setOnClickListener(this);
        btnNum9.setOnClickListener(this);
        btnDeleteNumber.setOnClickListener(this);
        btnDot.setOnClickListener(this);
        btnEqual.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnMultiply.setOnClickListener(this);
        btnDivide.setOnClickListener(this);
        btnFlagFrom.setOnClickListener(this);
        btnFlagTo.setOnClickListener(this);

        return view;
    }

    private void initialize(View view) {
        btnBackToHome = (ImageButton) view.findViewById(R.id.btnBackToHome);
        btnNum0 = (RelativeLayout) view.findViewById(R.id.btnNum0);
        btnNum1 = (RelativeLayout) view.findViewById(R.id.btnNum1);
        btnNum2 = (RelativeLayout) view.findViewById(R.id.btnNum2);
        btnNum3 = (RelativeLayout) view.findViewById(R.id.btnNum3);
        btnNum4 = (RelativeLayout) view.findViewById(R.id.btnNum4);
        btnNum5 = (RelativeLayout) view.findViewById(R.id.btnNum5);
        btnNum6 = (RelativeLayout) view.findViewById(R.id.btnNum6);
        btnNum7 = (RelativeLayout) view.findViewById(R.id.btnNum7);
        btnNum8 = (RelativeLayout) view.findViewById(R.id.btnNum8);
        btnNum9 = (RelativeLayout) view.findViewById(R.id.btnNum9);
        btnDot = (RelativeLayout) view.findViewById(R.id.btnDot);
        btnEqual = (RelativeLayout) view.findViewById(R.id.btnEqual);
        btnPlus = (RelativeLayout) view.findViewById(R.id.btnPlus);
        btnMinus = (RelativeLayout) view.findViewById(R.id.btnMinus);
        btnMultiply = (RelativeLayout) view.findViewById(R.id.btnMultiply);
        btnDivide = (RelativeLayout) view.findViewById(R.id.btnDivide);
        edtFrom = (EditText) view.findViewById(R.id.edtConvertFrom);
        edtTo = (EditText) view.findViewById(R.id.edtConvertTo);
        btnDeleteNumber = (ImageView) view.findViewById(R.id.btnDeleteNumber);
        btnFlagFrom = (ImageView) view.findViewById(R.id.imageFlagFrom);
        btnFlagTo = (ImageView) view.findViewById(R.id.imageFlagTo);

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray countries=obj.getJSONArray("countries");

            for (int i = 0; i < countries.length(); i++) {
                JSONObject jsonObject=countries.getJSONObject(i);
                String key = jsonObject.getString("name");
                String value = jsonObject.getString("key");
                contextMenus.add(new MoneyMoneyClasses.ContextMenu(value + " - " + key, value));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialogFrom = initializeCountryFrom();
        dialogTo = initializeCountryTo();
    }

    private String loadJSONFromAsset() {
        String json = null;

        try {
            InputStream is = getActivity().getAssets().open("currentcy.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }

    private AlertDialog initializeCountryFrom() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View popup = getActivity().getLayoutInflater().inflate(R.layout.custom_context_menu, null);
        builder.setView(popup);

        final ListView listView = (ListView) popup.findViewById(R.id.listView);

        final ContextMenuAdapter adapter = new ContextMenuAdapter(getContext(), R.layout.context_menu, contextMenus);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) view.findViewById(R.id.contextMenuValue);
                String text = textView.getText ().toString ();
                Logger.d ( "TEXT" + text );
                dialogFrom.dismiss();
                try {
                    JSONObject obj = new JSONObject(loadJSONFromAsset());
                    JSONArray countries=obj.getJSONArray("countries");
                    //Logger.d ( "FRom" + from );
                    for (int f=0;f < countries.length(); f++) {

                        JSONObject jsonObject=countries.getJSONObject(f);
                        String value1 = jsonObject.getString("key");
                        if (value1.equals ( text )){
                            String varImage = jsonObject.getString("image").substring(0, jsonObject.getString("image").lastIndexOf('.'));
                            int resId = getResources().getIdentifier(varImage, "drawable" , getContext ().getPackageName ());
                            btnFlagFrom.setImageResource ( resId );
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        builder.setView(popup);
        return builder.create();
    }

    private AlertDialog initializeCountryTo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View popup = getActivity().getLayoutInflater().inflate(R.layout.custom_context_menu, null);
        builder.setView(popup);

        final ListView listView = (ListView) popup.findViewById(R.id.listView);

        final ContextMenuAdapter adapter = new ContextMenuAdapter(getContext(), R.layout.context_menu, contextMenus);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) view.findViewById(R.id.contextMenuValue);
                String text = textView.getText ().toString ();
                Logger.d ( "TEXT" + text );
                dialogTo.dismiss();
                try {
                    JSONObject obj = new JSONObject(loadJSONFromAsset());
                    JSONArray countries=obj.getJSONArray("countries");

                    for (int f=0;f < countries.length(); f++) {
                        JSONObject jsonObject=countries.getJSONObject(f);
                        String value2 = jsonObject.getString("key");
                        if (value2.equals ( text )){
                            String varImage = jsonObject.getString("image").substring(0, jsonObject.getString("image").lastIndexOf('.'));
                            int resId = getResources().getIdentifier(varImage, "drawable" , getContext ().getPackageName ());
                            btnFlagTo.setImageResource ( resId );
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        builder.setView(popup);
        return builder.create();
    }

    private void resetStatusButtonCalculate() {
        btnPlus.setBackgroundColor(getResources().getColor(R.color.colorDarkGrey));
        btnMinus.setBackgroundColor(getResources().getColor(R.color.colorDarkGrey));
        btnMultiply.setBackgroundColor(getResources().getColor(R.color.colorDarkGrey));
        btnDivide.setBackgroundColor(getResources().getColor(R.color.colorDarkGrey));
    }

    private void resetStatusEditText() {
        first = false;
        valueB="";
        edtFrom.setText(valueB);
    }

    private void resetAll() {
        valueA = "";
        valueB = "";
        btnCalculate = "";
        result = "";
        edtFrom.setText(valueB);
        edtTo.setText("");
        first = true;
        resetStatusButtonCalculate();
    }

    private void updateTextAndValue(String string) {
        Logger.d("BEFORE: Value A: " + valueA + "|Value B: " + valueB + "|btnCalculate: " + btnCalculate);

        if (!result.isEmpty()) {
            resetAll();
        }

        if (!edtTo.getText().toString().isEmpty()) edtTo.setText("");

        edtFrom.setText(edtFrom.getText() + string);

        valueB += string;

        if (first) {
            valueA = valueB;
        }else {
            if (btnCalculate.isEmpty()) {
                valueA = valueA + valueB;
            }else {
                valueA = valueA + btnCalculate + valueB;
            }

            btnCalculate = "";
            valueB="";
        }

        Logger.d("AFTER: Value A: " + valueA + "|Value B: " + valueB + "|btnCalculate: " + btnCalculate);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.imageFlagFrom:
                dialogFrom.show();
                break;
            case R.id.imageFlagTo:
                dialogTo.show();
                break;
            case R.id.btnNum0: {
                updateTextAndValue("0");
                break;
            }
            case R.id.btnNum1: {
                updateTextAndValue("1");
                break;
            }
            case R.id.btnNum2: {
                updateTextAndValue("2");
                break;
            }
            case R.id.btnNum3: {
                updateTextAndValue("3");
                break;
            }
            case R.id.btnNum4: {
                updateTextAndValue("4");
                break;
            }
            case R.id.btnNum5: {
                updateTextAndValue("5");
                break;
            }
            case R.id.btnNum6: {
                updateTextAndValue("6");
                break;
            }
            case R.id.btnNum7: {
                updateTextAndValue("7");
                break;
            }
            case R.id.btnNum8: {
                updateTextAndValue("8");
                break;
            }
            case R.id.btnNum9: {
                updateTextAndValue("9");
                break;
            }
            case R.id.btnDeleteNumber: {
                resetAll();
                break;
            }
            case R.id.btnDot: {
                updateTextAndValue(".");
                break;
            }
            case R.id.btnEqual: {
                /* https://api.ofx.com/PublicSite.ApiService/OFX/spotrate/Individual/USD/VND/1?format=json */
                if (!valueA.isEmpty() && btnCalculate.isEmpty()) {
                    try {
                        result = (String) engine.eval(valueA).toString();
                        String host = "https://api.ofx.com/PublicSite.ApiService/OFX/spotrate/Individual/" + from + "/" + to + "/" + result + "?format=json";
                        AsyncHttpClient client = new AsyncHttpClient();
                        client.get(host, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                Logger.d("MONEY: " + response);
                                try {
                                    String converted = response.getString("InterbankAmount");
                                    edtTo.setText(String.valueOf(Float.parseFloat(converted)));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                StaticClass.ShowAlertDialog(getContext(), "Error", getResources().getString(R.string.err_convert), null, null, "OK", null);
                            }
                        });
                    } catch (ScriptException e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case R.id.btnPlus: {
                resetStatusButtonCalculate();
                btnPlus.setBackgroundColor(getResources().getColor(R.color.colorDarkGreyActive));

                btnCalculate = "+";
                resetStatusEditText();
                break;
            }
            case R.id.btnMinus: {
                resetStatusButtonCalculate();
                btnMinus.setBackgroundColor(getResources().getColor(R.color.colorDarkGreyActive));

                btnCalculate = "-";
                resetStatusEditText();
                break;
            }
            case R.id.btnMultiply: {
                resetStatusButtonCalculate();
                btnMultiply.setBackgroundColor(getResources().getColor(R.color.colorDarkGreyActive));

                btnCalculate = "*";
                resetStatusEditText();
                break;
            }
            case R.id.btnDivide: {
                resetStatusButtonCalculate();
                btnDivide.setBackgroundColor(getResources().getColor(R.color.colorDarkGreyActive));

                btnCalculate = "/";
                resetStatusEditText();
                break;
            }
        }
    }
}