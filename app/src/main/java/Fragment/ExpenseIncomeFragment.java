package Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.dangkhoa.moneymoney.R;
import com.dangkhoa.moneymoney.SignInActivity;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.CategoryAdapter;
import MoneyMoneyClasses.CategoryItem;
import MoneyMoneyClasses.StaticClass;
import cz.msebera.android.httpclient.Header;

/**
 * Created by dangkhoa on 7/30/17.
 */

public class ExpenseIncomeFragment extends android.support.v4.app.Fragment implements View.OnClickListener {
    RelativeLayout moneyNav, dialogLoading;
    PieChart pie_chart;
    ImageButton btnNew;
    RecyclerView recyclerView;
    TextView datePicker;
    PullRefreshLayout pullRefreshLayout;
    TextView cYear, cJanuary, cFebruary, cMarch, cApril, cMay, cJune, cJuly, cAugust, cSeptember, cOctorber, cNovember, cDecember;
    ImageButton cBack, cNext;

    float rainfall[] = {89000, 89000, 89000, 89000, 89000, 89000, 89000, 89000, 89000};
    String monthNames[] = {"Food", "Education", "Sport", "Household", "Health", "Travel", "Fashion", "Transport", "Etc"};

    ArrayList<CategoryItem> categoryItems = new ArrayList<>();
    String type;

    Calendar now = Calendar.getInstance();
    Bundle bundle = new Bundle();
    AlertDialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_expense_income, container, false);

        System.gc();

        RequestParams params = new RequestParams();
        params.put("id", StaticClass.getData(getContext(), "id"));

        final String typeBundle = StaticClass.getData(getContext(), "type");
        Logger.d("BUNDLE: " + StaticClass.getData(getContext(), "type"));
        if (typeBundle != null) {
            type = StaticClass.getData(getContext(), "type");
        }

        initialize(view, type);

        btnNew.setOnClickListener(this);
        datePicker.setOnClickListener(this);
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sortByMonthOrYear();
            }
        });

        return  view;
    }

    private void initialize(View view, final String type) {
        pie_chart = (PieChart) view.findViewById(R.id.pie_chart);
        moneyNav = (RelativeLayout) view.findViewById(R.id.moneyNav);
        moneyNav.addView(StaticClass.initNavButton(view.getContext(), view));
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_category);
        btnNew = (ImageButton) view.findViewById(R.id.btnNew);
        datePicker = (TextView) view.findViewById(R.id.datePicker);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefreshLayout);
        dialogLoading = (RelativeLayout) view.findViewById(R.id.dialogLoading);

        if (now.get(Calendar.MONTH) + 1 < 10) {
            datePicker.setText("0" + String.valueOf(now.get(Calendar.MONTH) + 1) + "/" + String.valueOf(now.get(Calendar.YEAR)));
        }else {
            datePicker.setText(String.valueOf(now.get(Calendar.MONTH) + 1 + "/" + now.get(Calendar.YEAR)));
        }

        dialog = initializeCalendar();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                sortByMonthOrYear();
            }
        });

        if (CardManageFragment.cards.size() == 0) {
            RequestParams params = new RequestParams();
            params.put("id", StaticClass.getData(getContext(), "id"));
            AsyncHttp.GET("/cards", params, StaticClass.getToken(getContext()), new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        JSONArray array = response.getJSONArray("successData");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            CardManageFragment.cards.add(new MoneyMoneyClasses.ContextMenu(object.getString("name"), object.getString("name") + " - " + object.getString("number"), object.getString("_id")));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }
            });
        }

        sortByMonthOrYear();
    }

    private void setUpChart(PieChart pie_chart, String type) {
        float sum = 0;
        List<PieEntry> pieEntryList = new ArrayList<>();

        for (int i = 0; i < categoryItems.size(); i++) {
            pieEntryList.add(new PieEntry(
                    Float.parseFloat(StaticClass.ConvertDecimalToString(categoryItems.get(i).getPrice())),
                    categoryItems.get(i).getCategory())
            );
            sum += Float.parseFloat(StaticClass.ConvertDecimalToString(categoryItems.get(i).getPrice()));
        }

        PieDataSet pieDataSet = new PieDataSet(pieEntryList, null);
        PieData pieData = new PieData(pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());
        pie_chart.setData(pieData);
        pie_chart.invalidate();
        pie_chart.setUsePercentValues(true);

        String center = WordUtils.capitalize(type + "\n" + StaticClass.ConvertStringToDecimal(sum));
        SpannableString ss = new SpannableString(center);

        if (type.equals("expense")) {
            ss.setSpan(new RelativeSizeSpan(0.75f), 0, 7, 0);
        }else {
            ss.setSpan(new RelativeSizeSpan(0.75f), 0, 6, 0);
        }

        pie_chart.setCenterText(ss);

        pie_chart.setCenterTextSize(22);

        pie_chart.setDrawHoleEnabled(true);
        pie_chart.setHoleRadius(80);

        pieDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        pieData.setValueTextSize(14f);
        pieData.setValueTextColor(Color.DKGRAY);


        pie_chart.animateXY(1000, 1000);
        pie_chart.setDescription(null);
        pie_chart.setEntryLabelTextSize(12f);

        pie_chart.setRotationAngle(0);
        pie_chart.setRotationEnabled(true);

        pieDataSet.setSliceSpace(3f);
        pieDataSet.setSelectionShift(5f);

        pieDataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        pieDataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        pieDataSet.setValueLinePart1Length(0.2f);
        pieDataSet.setValueLinePart2Length(0.35f);
        pieDataSet.setValueLineColor(getResources().getColor(R.color.colorDarkGrey));

        pie_chart.setExtraTopOffset(20f);
        pie_chart.setExtraBottomOffset(20f);
        pie_chart.setExtraLeftOffset(35f);
        pie_chart.setExtraRightOffset(35f);

        pie_chart.getLegend().setEnabled(false);

        pie_chart.setHoleColor(getResources().getColor(R.color.colorPrimary));
        pie_chart.setCenterTextColor(getResources().getColor(R.color.colorWhite));
    }

    private void setUpRecyclerViewList(RecyclerView recyclerView) {
        Collections.sort(categoryItems, new Comparator<CategoryItem>() {
            @Override
            public int compare(CategoryItem t1, CategoryItem t2) {
                return t1.getCategory().compareTo(t2.getCategory());
            }
        });
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.custom_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);

        CategoryAdapter categoryAdapter = new CategoryAdapter(categoryItems, getContext());
        categoryAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(categoryAdapter);
    }

    private void requestToServer(final String type, final int month, @NonNull final int year) {
        dialogLoading.setVisibility(View.VISIBLE);

        String id = StaticClass.getData(getContext(), "id");
        Logger.d("ID:" +id);
        RequestParams params = new RequestParams();
        params.put("id", id);

        AsyncHttp.GET("/records", params, StaticClass.getToken(getContext()), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                categoryItems.clear();
                try {
                    JSONArray records = response.getJSONArray("successData");
                    for (int i = 0; i < records.length(); i++) {
                        JSONObject object = (JSONObject) records.get(i);

                        Logger.d("RECORD: " + i + ": " + object);

                        if (object.getJSONObject("_id").getString("type").equals(type)) {
                            if (month > 0 && month == object.getInt("month") && year == object.getInt("year")) {
                                String category_name = object.getJSONObject("_id").getString("category");
                                categoryItems.add(new CategoryItem(
                                        StaticClass.getIconDetail(getContext(), category_name),
                                        category_name,
                                        StaticClass.ConvertStringToDecimal(object.getString("sum"))));
                            }else if (month <= 0 && year == object.getInt("year")) {
                                String category_name = object.getJSONObject("_id").getString("category");
                                categoryItems.add(new CategoryItem(
                                        StaticClass.getIconDetail(getContext(), category_name),
                                        category_name,
                                        StaticClass.ConvertStringToDecimal(object.getString("sum"))));
                            }
                        }
                    }

                    setUpChart(pie_chart, type);
                    setUpRecyclerViewList(recyclerView);

                    pullRefreshLayout.setRefreshing(false);
                    dialogLoading.setVisibility(View.INVISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    pullRefreshLayout.setRefreshing(false);
                    dialogLoading.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Logger.d("STATUS_CODE: " + String.valueOf(statusCode));
                pullRefreshLayout.setRefreshing(false);
                dialogLoading.setVisibility(View.INVISIBLE);
                switch (statusCode) {
                    case 0:
                        StaticClass.ShowAlertDialog(getContext(), "Error", "Something went wrong.", null, null, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getActivity().finish();
                            }
                        });
                        break;
                    case 401: {
                        StaticClass.removeData(getContext(), "token");
                        startActivity(new Intent(getContext(), SignInActivity.class));
                        getActivity().finish();
                        break;
                    }
                    default:
                        StaticClass.ShowAlertDialog(getContext(), "Error", "Something went wrong.", null, null, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getActivity().finish();
                            }
                        });
                        break;

                }
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }

    private void sortByMonthOrYear() {
        if (datePicker.getText().toString().indexOf('/') == 2) {
            int month = Integer.parseInt(String.valueOf(datePicker.getText()).substring(0, 2));
            int year = Integer.parseInt(String.valueOf(datePicker.getText()).substring(3, datePicker.getText().length()));
            Logger.d("DATE: " + String.valueOf(year));
            requestToServer(type, month, year);
        }else {
            int year = Integer.parseInt(String.valueOf(datePicker.getText()));
            requestToServer(type, 0, year);
        }
    }

    private AlertDialog initializeCalendar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View popup = getActivity().getLayoutInflater().inflate(R.layout.custom_calendar, null);

        /* Connect */
        cYear = (TextView) popup.findViewById(R.id.cYear);
        cJanuary = (TextView) popup.findViewById(R.id.cJanuary);
        cFebruary = (TextView) popup.findViewById(R.id.cFebruary);
        cMarch = (TextView) popup.findViewById(R.id.cMarch);
        cApril = (TextView) popup.findViewById(R.id.cApril);
        cMay = (TextView) popup.findViewById(R.id.cMay);
        cJune = (TextView) popup.findViewById(R.id.cJune);
        cJuly = (TextView) popup.findViewById(R.id.cJuly);
        cAugust = (TextView) popup.findViewById(R.id.cAugust);
        cSeptember = (TextView) popup.findViewById(R.id.cSeptember);
        cOctorber = (TextView) popup.findViewById(R.id.cOctorber);
        cNovember = (TextView) popup.findViewById(R.id.cNovember);
        cDecember = (TextView) popup.findViewById(R.id.cDecember);
        cBack = (ImageButton) popup.findViewById(R.id.cBack);
        cNext = (ImageButton) popup.findViewById(R.id.cNext);
        /* Connect */

        /* Config */
        cYear.setText(String.valueOf(now.get(Calendar.YEAR)));
        switch (now.get(Calendar.MONTH)) {
            case 0:
                cJanuary.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 1:
                cFebruary.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 2:
                cMarch.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 3:
                cApril.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 4:
                cMay.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 5:
                cJune.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 6:
                cJuly.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 7:
                cAugust.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 8:
                cSeptember.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 9:
                cOctorber.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 10:
                cNovember.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 11:
                cDecember.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
        }
        /* Config */

        /* Set click listener on each month */
        cYear.setOnClickListener(this);
        cJanuary.setOnClickListener(this);
        cFebruary.setOnClickListener(this);
        cMarch.setOnClickListener(this);
        cApril.setOnClickListener(this);
        cMay.setOnClickListener(this);
        cJune.setOnClickListener(this);
        cJuly.setOnClickListener(this);
        cAugust.setOnClickListener(this);
        cSeptember.setOnClickListener(this);
        cOctorber.setOnClickListener(this);
        cNovember.setOnClickListener(this);
        cDecember.setOnClickListener(this);
        cBack.setOnClickListener(this);
        cNext.setOnClickListener(this);
        /* Set click listener on each month */

        builder.setView(popup);
        return builder.create();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.datePicker:
                dialog.show();
                break;
            case R.id.cBack: {
                int year = Integer.parseInt(String.valueOf(cYear.getText()));
                year -= 1;
                cYear.setText(String.valueOf(year));
                break;
            }
            case R.id.cNext: {
                int year = Integer.parseInt(String.valueOf(cYear.getText()));
                year += 1;
                cYear.setText(String.valueOf(year));
                break;
            }
            case R.id.cYear:
                datePicker.setText(cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cJanuary:
                datePicker.setText("01/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cFebruary:
                datePicker.setText("02/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cMarch:
                datePicker.setText("03/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cApril:
                datePicker.setText("04/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cMay:
                datePicker.setText("05/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cJune:
                datePicker.setText("06/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cJuly:
                datePicker.setText("07/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cAugust:
                datePicker.setText("08/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cSeptember:
                datePicker.setText("09/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cOctorber:
                datePicker.setText("10/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cNovember:
                datePicker.setText("11/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.cDecember:
                datePicker.setText("12/" + cYear.getText());
                dialog.dismiss();
                break;
            case R.id.btnNew:
                bundle.putString("state", "ADD_NEW");
                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), false, new AddNewExpenseOrIncome(), bundle);
                break;
        }
    }
}